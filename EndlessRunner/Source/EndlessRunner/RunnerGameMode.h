// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunnerGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNER_API ARunnerGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	FVector Location = FVector(0);

protected:
	virtual void BeginPlay() override;
	FTimerHandle TimerHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ATile> tile;

	UFUNCTION()
		void OnTileExited(ATile* SpawnedTile);
	
	UFUNCTION()
		void SpawnTile();

	UFUNCTION()
		void DestroyTile(ATile* DestroyedTile);
};
