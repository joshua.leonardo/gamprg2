// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();
	runCharacter = Cast<ARunCharacter>(this->GetCharacter());
}


void ARunCharacterController::MoveRight(float scale)
{
	runCharacter->AddMovementInput(runCharacter->GetActorRightVector() * scale);
}

// Called every frame
void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (runCharacter->isDead == false)
	{
		runCharacter->AddMovementInput(runCharacter->GetActorForwardVector());
	}
}

// Called to bind functionality to input
void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}