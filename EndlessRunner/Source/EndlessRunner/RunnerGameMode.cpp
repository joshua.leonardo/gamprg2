// Fill out your copyright notice in the Description page of Project Settings.


#include "RunnerGameMode.h"
#include "RunCharacter.h"
#include "Tile.h"
#include "TimerManager.h"
#include "Engine/World.h"


void ARunnerGameMode::BeginPlay()
{
	Super::BeginPlay();

	for (int i = 0; i < 15; i++)
	{
		SpawnTile();
	}
}

void ARunnerGameMode::SpawnTile()
{
	FActorSpawnParameters SpawnParameters;

	ATile* Tile = GetWorld()->SpawnActor<ATile>(tile, Location, FRotator(0), SpawnParameters);
	Location = Tile->GetAttachTransform() + FVector(500, 0, 0);

	Tile->OnCollision.AddDynamic(this, &ARunnerGameMode::OnTileExited);
}

void ARunnerGameMode::DestroyTile(ATile* DestroyedTile)
{
	DestroyedTile->Destroy();
}

void ARunnerGameMode::OnTileExited(ATile* SpawnedTile)
{
	SpawnTile();
	GetWorldTimerManager().SetTimer(TimerHandle, FTimerDelegate::CreateUObject(this, &ARunnerGameMode::DestroyTile, SpawnedTile), .5f, false);
}






