// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacter.h"
#include "RunCharacterController.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
ARunCharacter::ARunCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	SpringArm->SetupAttachment(GetMesh());
	SpringArm->TargetArmLength = 500.0f;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(SpringArm);
	
}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void ARunCharacter::Die()
{
	if (isDead == false)
	{
		isDead = true;;
		this->GetMesh()->SetVisibility(false);
		this->GetController()->DisableInput(Cast<ARunCharacterController>(this->GetController()));
		OnDeath.Broadcast(this);
	}
}

void ARunCharacter::AddCoin()
{
	totalCoins += 1;
}


