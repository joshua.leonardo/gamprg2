// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacter.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNER_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()
	
	protected:
	virtual void BeginPlay() override;
	ARunCharacter* runCharacter;

	void MoveRight(float scale);

	public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

};


