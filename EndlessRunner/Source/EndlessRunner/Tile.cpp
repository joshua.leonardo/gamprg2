// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "RunCharacter.h"
#include "Obstacle.h"
#include "Pickup.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);

	AttachPoint = CreateDefaultSubobject<UArrowComponent>("AttachPoint");
	AttachPoint->SetupAttachment(SceneComponent);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>("ExitTrigger");
	ExitTrigger->SetupAttachment(SceneComponent);
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnBeginOverlap);

	ObstacleTrigger = CreateDefaultSubobject<UBoxComponent>("ObstacleTrigger");
	ObstacleTrigger->SetupAttachment(SceneComponent);

	PickupTrigger = CreateDefaultSubobject<UBoxComponent>("PickupTrigger");
	PickupTrigger->SetupAttachment(SceneComponent);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	
	if (FMath::RandRange(1, 100) <= 40) {
		SpawnObstacle();
	}
	if (FMath::RandRange(1, 100) <= 30) {
		SpawnPickup();
	}
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATile::SpawnObstacle()
{
	FActorSpawnParameters SpawnParameters;

	AObstacle* obstacle = GetWorld()->SpawnActor<AObstacle>(obstacles[0], UKismetMathLibrary::RandomPointInBoundingBox(ObstacleTrigger->GetRelativeLocation(), ObstacleTrigger->GetScaledBoxExtent()),  FRotator(0), SpawnParameters);

	obstacle->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

	spawnedObjects.Add(obstacle);
}

void ATile::SpawnPickup()
{
	FActorSpawnParameters SpawnParameters;

	APickup* pickup = GetWorld()->SpawnActor<APickup>(pickups[0], UKismetMathLibrary::RandomPointInBoundingBox(PickupTrigger->GetRelativeLocation(), PickupTrigger->GetScaledBoxExtent()), FRotator(90, 0, 0), SpawnParameters);

	pickup->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	
	spawnedObjects.Add(pickup);
}

void ATile::DestroyObjects()
{
	for (AActor* spawned:spawnedObjects)
	{
		spawned->Destroy();																				
	}
}

void ATile::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* runCharacter = Cast<ARunCharacter>(OtherActor))
	{
		OnCollision.Broadcast(this);
		DestroyObjects();
	}
}

FVector ATile::GetAttachTransform()
{
	return AttachPoint->GetComponentLocation();
}

